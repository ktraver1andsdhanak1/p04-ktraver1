//
//  GameScene.swift
//  teamMario
//
//  Created by kevin travers on 2/17/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.
//

import SpriteKit
//all images found online i did not make anyofthem
class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // Variables
    var player = SKSpriteNode()
    var flag = SKSpriteNode()
    var pole = SKSpriteNode()
    var left = SKSpriteNode()
    var right = SKSpriteNode()
    var jump = SKSpriteNode()
    
    // GamePlay, and Score
    var gamePlay: Bool = true
    var score: Int = 0
    var scoreNode = SKLabelNode()
    
    // Camera
    var myCamera = SKCameraNode()
    var cameraDistanceFromPlayerX = CGFloat()
    var cameraDistanceFromPlayerY = CGFloat()
    var counter = 1
    // player States
    enum playerStates: Int {
        case runningRight = 1
        case jumpWhileRunningRight = 2
        case standFacingRight = 3
        case jumpWhileStandingAndFacingRight = 4
        case runningLeft = 5
        case jumpWhileRunningLeft = 6
        case standFacingLeft = 7
        case jumpWhileStandingAndFacingLeft = 8
    }
    
    // Object Z Positions
    enum objectZPositions: CGFloat {
        case background = 0
        case obstacles = 1
        case ground = 2
        case gap = 3
        case enemyBoundary = 4
        case coins = 5
        case enemy = 6
        case controls = 7
        case score = 8
        case player = 9
        case pole = 10
        case flag = 11
        case labelNodes = 12
        case boundary = 13
        case travel = 14
        case camera = 15
        case button = 16
    }
    // Category Bit Masks
    
    let playerGroup: UInt32 = 0x1 << 0
    let groundGroup: UInt32 = 0x1 << 1
    let obstacleGroup: UInt32 = 0x1 << 2
    let enemyBoundaryGroup: UInt32 = 0x1 << 3
    let flagGroup: UInt32 = 0x1 << 4
    let poleGroup: UInt32 = 0x1 << 5
    let gapGroup: UInt32 = 0x1 << 6
    let enemyGroup: UInt32 = 0x1 << 7
    let coinsGroup: UInt32 = 0x1 << 8
    let boundaryGroup: UInt32 = 0x1 << 9
    let travelGroup: UInt32 = 0x1 << 10
    
    // Right Direction Textures
    let rightTexture1: SKTexture = SKTexture(imageNamed: "right1")
    let rightTexture2: SKTexture = SKTexture(imageNamed: "right2")
    let rightTexture3: SKTexture = SKTexture(imageNamed: "right3")
    let rightTexture4: SKTexture = SKTexture(imageNamed: "right4")
    let rightTextureStill: SKTexture = SKTexture(imageNamed: "rightstill")
    let rightTextureJump: SKTexture = SKTexture(imageNamed: "rightjump")
    
    // Left Direction Textures
    let leftTexture1: SKTexture = SKTexture(imageNamed: "left1")
    let leftTexture2: SKTexture = SKTexture(imageNamed: "left2")
    let leftTexture3: SKTexture = SKTexture(imageNamed: "left3")
    let leftTexture4: SKTexture = SKTexture(imageNamed: "left4")
    let leftTextureStill: SKTexture = SKTexture(imageNamed: "leftstill")
    let leftTextureJump: SKTexture = SKTexture(imageNamed: "leftjump")
    
    var playerStatus = Int()
    
    var isJumping: Bool = false

    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        //create pyhsics for the owlrd and allows for contacts between two objects to be detected
        //physicsWorld.gravity = CGVectorMake(0,-1)
        self.physicsWorld.contactDelegate = self
        
        //get access to background
        let background = childNodeWithName("background") as! SKSpriteNode
        //set z postion of background
        background.zPosition = objectZPositions.background.rawValue
     
        
        
        //create player and set physics and collison for player
        let playerSpawn = childNodeWithName("playerSpawn")
        player = SKSpriteNode(texture: SKTexture(imageNamed: "player"))
        //starting postion is where ever spawn is
        player.position = (playerSpawn?.position)!
        playerSpawn?.removeFromParent()
        player.name = "player"
        //give player physics
        player.physicsBody = SKPhysicsBody(rectangleOfSize: player.size)
        player.setScale(0.2)
        player.physicsBody?.mass = 0.09
        //gravity affects the player
        //player.physicsBody?.affectedByGravity = true
        //does not rotate
        player.physicsBody?.allowsRotation = false
        //set z postion so not behind background
        player.zPosition = objectZPositions.player.rawValue
        //set up collison and contact detection
        //player in player group
        player.physicsBody?.categoryBitMask = playerGroup
        player.physicsBody?.collisionBitMask = groundGroup | obstacleGroup | enemyGroup | flagGroup | travelGroup | boundaryGroup
        player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup | travelGroup
        addChild(player)
        //set statsus
        playerStatus = playerStates.standFacingRight.rawValue
        //set up camera
        myCamera = childNodeWithName("camera") as! SKCameraNode
        myCamera.zPosition = objectZPositions.camera.rawValue
        //cameraDistanceFromPlayer = 160.0
        //scoring system settup
        scoreNode = myCamera.childNodeWithName("score") as! SKLabelNode
        scoreNode.zPosition = objectZPositions.score.rawValue
        
        //set up gaps between grounds
       
        self.enumerateChildNodesWithName("//gap_[0-9]*") { (node, stop) -> Void in
            
            node.physicsBody?.categoryBitMask = self.gapGroup
            node.physicsBody?.collisionBitMask = self.playerGroup
            node.physicsBody?.contactTestBitMask = self.playerGroup
            node.zPosition = objectZPositions.gap.rawValue
            node.alpha = 0
            
            
        }
       
        
        self.enumerateChildNodesWithName("//enemyObstacle_[0-9]*") { (node, stop) -> Void in
            
            node.physicsBody?.categoryBitMask = self.enemyBoundaryGroup
            node.physicsBody?.collisionBitMask = self.enemyGroup
            node.physicsBody?.contactTestBitMask = self.enemyGroup
            node.zPosition = objectZPositions.enemyBoundary.rawValue
            node.alpha = 0
            
            
        }
        
        // Ground Gaps
        
        
        // Player Boundaries
        
        self.enumerateChildNodesWithName("//boundary_[0-9]*") { (node, stop) -> Void in
            
            node.physicsBody?.categoryBitMask = self.boundaryGroup
            node.physicsBody?.collisionBitMask = self.playerGroup
            node.zPosition = objectZPositions.boundary.rawValue
            node.alpha = 0
        }
        
        // Setup Obstacles
        self.enumerateChildNodesWithName("//obstacle_[0-9]*") { (node, stop) -> Void in
            
            node.physicsBody?.categoryBitMask = self.obstacleGroup
            node.physicsBody?.collisionBitMask = self.enemyGroup | self.playerGroup
            node.physicsBody?.contactTestBitMask =  self.enemyGroup | self.playerGroup
            node.zPosition = objectZPositions.obstacles.rawValue
            
            
        }
        self.enumerateChildNodesWithName("//travel_[0-9]*") { (node, stop) -> Void in
            
            node.physicsBody?.categoryBitMask = self.travelGroup
            node.physicsBody?.collisionBitMask = self.enemyGroup | self.playerGroup
            node.physicsBody?.contactTestBitMask =  self.playerGroup
            node.zPosition = objectZPositions.travel.rawValue
            
            
        }
        
        // Setup Ground
        
        self.enumerateChildNodesWithName("//ground_[0-9]*") { (node, stop) -> Void in
            
            node.zPosition = objectZPositions.ground.rawValue
            node.physicsBody?.categoryBitMask = self.groundGroup
            node.physicsBody?.collisionBitMask = self.playerGroup | self.enemyGroup | self.obstacleGroup |  self.poleGroup
            node.physicsBody?.contactTestBitMask = self.playerGroup
            
            
        }
        
        // Setup Flag and Pole
        flag = childNodeWithName("flag") as! SKSpriteNode
        flag.zPosition = objectZPositions.flag.rawValue
        flag.physicsBody?.categoryBitMask = flagGroup
        flag.physicsBody?.contactTestBitMask = playerGroup
        flag.physicsBody?.collisionBitMask = playerGroup
        
        //pole = childNodeWithName("pole") as! SKSpriteNode
        pole = childNodeWithName("pole") as! SKSpriteNode
        pole.zPosition = objectZPositions.pole.rawValue
        pole.physicsBody?.categoryBitMask = poleGroup
        pole.physicsBody?.contactTestBitMask = playerGroup
        //pole.physicsBody?.collisionBitMask = playerGroup
        
        
        // Setup Coins
        
        self.enumerateChildNodesWithName("//coin_[0-9]*") { (node, stop) -> Void in
            
            node.zPosition = objectZPositions.coins.rawValue
            node.physicsBody?.categoryBitMask = self.coinsGroup
            node.physicsBody?.contactTestBitMask = self.playerGroup
            
        }
        
        // For Camera Movement
        cameraDistanceFromPlayerX = myCamera.position.x - player.position.x
        cameraDistanceFromPlayerY = myCamera.position.y - player.position.y + 200
        
        // Enemies
        self.enumerateChildNodesWithName("//spawnPoint_[0-9]*") { (node, stop) -> Void in
            
            let enemy = Enemy(initialPosition: node.position)
            node.removeFromParent()
            
            enemy.physicsBody = SKPhysicsBody(texture: enemy.texture!, alphaThreshold: 0, size: (enemy.texture?.size())!)
            enemy.size = CGSize(width: CGFloat(40), height: CGFloat(40))
            enemy.physicsBody?.categoryBitMask = self.enemyGroup
            enemy.physicsBody?.collisionBitMask = self.playerGroup | self.groundGroup | self.obstacleGroup | self.enemyBoundaryGroup | self.travelGroup
            enemy.physicsBody?.contactTestBitMask = self.playerGroup | self.obstacleGroup |  self.enemyBoundaryGroup
            enemy.physicsBody?.allowsRotation = false
            enemy.zPosition = objectZPositions.enemy.rawValue
            self.addChild(enemy)
            
        
        
        }
        right = myCamera.childNodeWithName("right") as! SKSpriteNode
        right.zPosition = objectZPositions.button.rawValue
        
        right.alpha = 0.1
        //addChild(right)
        left = myCamera.childNodeWithName("left") as! SKSpriteNode
        left.zPosition = objectZPositions.button.rawValue
        left.alpha = 0.1
        //addChild(left)
        jump = myCamera.childNodeWithName("jump") as! SKSpriteNode
        jump.zPosition = objectZPositions.button.rawValue
        jump.alpha = 0.1
        //addChild(jump)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        let touch = touches.first!
        let location = touch.locationInNode(self)
        let nodeTouched = self.nodeAtPoint(location)
        if nodeTouched.name == "right"{
            //force player to be facing right
            move("facingRight")
            playerStatus = playerStates.runningRight.rawValue
        }else if nodeTouched.name == "left"{
            //force player to be facing left
            move("facingLeft")
            playerStatus = playerStates.runningLeft.rawValue
        }else if nodeTouched.name == "jump"{
            
            if playerStatus == playerStates.standFacingRight.rawValue {
               
                startJump("standingRight")
                playerStatus = playerStates.jumpWhileStandingAndFacingRight.rawValue
                
            } else if playerStatus == playerStates.standFacingLeft.rawValue {
                
                startJump("standingLeft")
                playerStatus = playerStates.jumpWhileStandingAndFacingLeft.rawValue
                
            } else if playerStatus == playerStates.runningLeft.rawValue {
                
                startJump("movingLeft")
                playerStatus = playerStates.jumpWhileRunningLeft.rawValue
                
            } else if playerStatus == playerStates.runningRight.rawValue {
                
                startJump("movingRight")
                playerStatus = playerStates.jumpWhileRunningRight.rawValue
            }
            isJumping = true
        }
     
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let location = touch.locationInNode(self)
        let nodeTouched = self.nodeAtPoint(location)
        if nodeTouched.name == "right"{
            //stopMoving("facingRight")
            move("facingRight")
        }else if nodeTouched.name == "left"{
            //stopMoving("facingLeft")
            move("facingLeft")
        }else if nodeTouched.name == "jump"{
            
            if playerStatus == playerStates.standFacingRight.rawValue {
                
                startJump("standingRight")
                playerStatus = playerStates.jumpWhileStandingAndFacingRight.rawValue
                
            } else if playerStatus == playerStates.standFacingLeft.rawValue {
                
                startJump("standingLeft")
                playerStatus = playerStates.jumpWhileStandingAndFacingLeft.rawValue
                
            } else if playerStatus == playerStates.runningLeft.rawValue {
                
                startJump("movingLeft")
                playerStatus = playerStates.jumpWhileRunningLeft.rawValue
                
            } else if playerStatus == playerStates.runningRight.rawValue {
                
                startJump("movingRight")
                playerStatus = playerStates.jumpWhileRunningRight.rawValue
            }
            isJumping = true
        }

    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = touches.first!
        let location = touch.locationInNode(self)
        let nodeTouched = self.nodeAtPoint(location)
        if nodeTouched.name == "right"{
            stopMoving("facingRight")
        }else if nodeTouched.name == "left"{
            stopMoving("facingLeft")
        }else if nodeTouched.name == "jump"{
           
            if playerStatus == playerStates.standFacingRight.rawValue {
                
                startJump("standingRight")
                playerStatus = playerStates.jumpWhileStandingAndFacingRight.rawValue
                
            } else if playerStatus == playerStates.standFacingLeft.rawValue {
                
                startJump("standingLeft")
                playerStatus = playerStates.jumpWhileStandingAndFacingLeft.rawValue
                
            } else if playerStatus == playerStates.runningLeft.rawValue {
                
                startJump("movingLeft")
                playerStatus = playerStates.jumpWhileRunningLeft.rawValue
                
            } else if playerStatus == playerStates.runningRight.rawValue {
                
                startJump("movingRight")
                playerStatus = playerStates.jumpWhileRunningRight.rawValue
            }
            isJumping = true
        }

    }
   
    
    
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        myCamera.position.x = player.position.x + cameraDistanceFromPlayerX
        //if player.position.y > 400{
            myCamera.position.y = player.position.y + cameraDistanceFromPlayerY
       // }else{
             //myCamera.position.y = cameraDistanceFromPlayerY + 150
        //}
        
    }
    
    func coinCollected() {
        score += 10
        scoreNode.text = "SCORE : \(score)"
        
    }
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        var objectOne: SKPhysicsBody
        var objectTwo: SKPhysicsBody
        
        // lower category is always stored in object one
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            objectOne = contact.bodyA
            objectTwo = contact.bodyB
        } else {
            objectOne = contact.bodyB
            objectTwo = contact.bodyA
        }
        if ((contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == travelGroup)) || (contact.bodyB.categoryBitMask == playerGroup && contact.bodyA.categoryBitMask == travelGroup){
            print("hiting travel pipe")
            if contact.bodyA.node?.name == "player"{
                var node = contact.bodyA.node as! SKSpriteNode
                var nodePipe = contact.bodyB.node as! SKSpriteNode
                print(node.position.y - nodePipe.position.y)
                //print("time to teleport new lvl")
                
                if nodePipe.name == "travel_1"{
                    //print("teleporting to new lvl")
                    if (node.position.y - nodePipe.position.y) > 60{
                    let playerSpawn = childNodeWithName("teleportSpawn_1")
                    let newX = (playerSpawn?.position.x)!
                    let newY = (playerSpawn?.position.y)!
                    let teleport = SKAction.moveTo(CGPointMake(newX, newY), duration: 0.1)
                    player.physicsBody?.collisionBitMask = enemyGroup | flagGroup | boundaryGroup
                    player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup
                    node.runAction(teleport)
                    playerSpawn?.removeFromParent()
                    player.physicsBody?.collisionBitMask = groundGroup | obstacleGroup | enemyGroup | flagGroup | travelGroup | boundaryGroup
                    player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup | travelGroup
                    }
                }else if nodePipe.name == "travel_2"{
                    
                        //print("teleportingback")
                        let playerSpawn = childNodeWithName("teleportSpawn_2")
                        let newX = (playerSpawn?.position.x)!
                        let newY = (playerSpawn?.position.y)!
                        let teleport = SKAction.moveTo(CGPointMake(newX, newY), duration: 0.1)
                        player.physicsBody?.collisionBitMask = enemyGroup | flagGroup | boundaryGroup
                        player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup
                        node.runAction(teleport)
                        playerSpawn?.removeFromParent()
                        player.physicsBody?.collisionBitMask = groundGroup | obstacleGroup | enemyGroup | flagGroup | travelGroup | boundaryGroup
                        player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup | travelGroup
                    
                }
                
                

            }else{
                
                var node = contact.bodyB.node as! SKSpriteNode
                var nodePipe = contact.bodyA.node as! SKSpriteNode
                print(node.position.y - nodePipe.position.y)
                //print(node.name)
                //print(player.position.y - nodePipe.position.y)
                //if (node.position.y - nodePipe.position.y) > 60{
                     //player.position.x = -407.922
                     //player.position.y = -494.99
                //print("time to teleport new lvl")
                    if nodePipe.name == "travel_1"{
                       if (node.position.y - nodePipe.position.y) > 60{
                        let playerSpawn = childNodeWithName("teleportSpawn_1")
                        let newX = (playerSpawn?.position.x)!
                        let newY = (playerSpawn?.position.y)!
                        let teleport = SKAction.moveTo(CGPointMake(newX, newY), duration: 0.1)
                        player.physicsBody?.collisionBitMask = enemyGroup | flagGroup | boundaryGroup
                        player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup
                        node.runAction(teleport)
                        //playerSpawn?.removeFromParent()
                        player.physicsBody?.collisionBitMask = groundGroup | obstacleGroup | enemyGroup | flagGroup | travelGroup | boundaryGroup
                        player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup | travelGroup
                        }
                    }else if nodePipe.name == "travel_2"{
                            //print("teleporting back")
                            let playerSpawn = childNodeWithName("teleportSpawn_2")
                            let newX = (playerSpawn?.position.x)!
                            let newY = (playerSpawn?.position.y)!
                            let teleport = SKAction.moveTo(CGPointMake(newX, newY), duration: 0.1)
                            player.physicsBody?.collisionBitMask = enemyGroup | flagGroup | boundaryGroup
                            player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup
                            node.runAction(teleport)
                            //playerSpawn?.removeFromParent()
                            player.physicsBody?.collisionBitMask = groundGroup | obstacleGroup | enemyGroup | flagGroup | travelGroup | boundaryGroup
                            player.physicsBody?.contactTestBitMask = enemyGroup | flagGroup | poleGroup | gapGroup | coinsGroup | travelGroup
                        
                    }
                    
                    
                //}
            }
            
        }
        
        
      
        if ((contact.bodyA.categoryBitMask == enemyBoundaryGroup || contact.bodyA.categoryBitMask == obstacleGroup) && contact.bodyB.categoryBitMask == enemyGroup) {
            
            if contact.bodyA.categoryBitMask == enemyGroup {
                let enemyNode = contact.bodyA.node! as! Enemy
                enemyNode.move()
            }else {
                let enemyNode = contact.bodyB.node! as! Enemy
                enemyNode.move()
            }
            
        }
        
        // Jump
        if ((contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == groundGroup) || (contact.bodyB.categoryBitMask == playerGroup && contact.bodyA.categoryBitMask == groundGroup)){
            
            checkJumpContacts()
            
            
        }
        
        // Obstacles
        
        if ((contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == obstacleGroup) || (contact.bodyB.categoryBitMask == playerGroup && contact.bodyA.categoryBitMask == obstacleGroup)) {
            
            checkObstacleContacts()
            
        }
        
        // Enemy - Game Over
        if ((contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == enemyGroup) ||
        (contact.bodyB.categoryBitMask == playerGroup && contact.bodyA.categoryBitMask == enemyGroup) ){
            if contact.bodyB.node != nil{
                //print("hit enemy")
                if contact.bodyA.node?.name == "player"{
                    var node = contact.bodyA.node as! SKSpriteNode
                    var nodeEnemy = contact.bodyB.node as! SKSpriteNode
                    //print(node.position.y - nodeEnemy.position.y)
                    if (node.position.y - nodeEnemy.position.y) < 40{
                        
                        //node.removeFromParent()
                        //print("GG")
                        gameOver("Game Over")
                        
                    }
                    else{
                        nodeEnemy.removeFromParent()
                        coinCollected()
                        coinCollected()
                        //print("another one bites the dust")
                    }
                }else{
                    var node = contact.bodyB.node as! SKSpriteNode
                    var nodeEnemy = contact.bodyA.node as! SKSpriteNode
                    //print(node.position.y - nodeEnemy.position.y)
                    if (node.position.y - nodeEnemy.position.y) < 40{
                        
                        //node.removeFromParent()
                        //print("GG")
                        gameOver("Game Over")
                        
                    }
                    else{
                        nodeEnemy.removeFromParent()
                        coinCollected()
                        coinCollected()
                        //print("another one bites the dust")
                    }

                }
                
            }
            
            
        }
        
        // Ground Gap - Game Over
        
        if ((contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == gapGroup) || (contact.bodyB.categoryBitMask == playerGroup && contact.bodyA.categoryBitMask == gapGroup)) {
            //print("GG")
            gameOver("Game Over")
        }
        
        // Level Complete
        if ((contact.bodyA.categoryBitMask == playerGroup && (contact.bodyB.categoryBitMask == flagGroup || contact.bodyB.categoryBitMask == poleGroup)) ||
           (contact.bodyB.categoryBitMask == playerGroup && (contact.bodyA.categoryBitMask == flagGroup || contact.bodyA.categoryBitMask == poleGroup))){
            //print("GG")
            gameOver("Level Won")
        }
        
        
        
        // Coins
        if (contact.bodyA.categoryBitMask == playerGroup && contact.bodyB.categoryBitMask == coinsGroup){
            let node = contact.bodyB.node as! SKSpriteNode
            let move = SKAction.moveByX(0, y: 30, duration: 1.0)
            let scale = SKAction.scaleBy(0, duration: 1.0)
            node.runAction(SKAction.group([move, scale])) { () -> Void in
                node.removeFromParent()
            }
            
            coinCollected()
        }
  
    }
    func gameOver(gameOverStatus: String){
        if(gameOverStatus == "Game Over"){
            //print("you lose")
            if let scene = GameOverScene(fileNamed:"GameOverScene") {
                // Configure the view.
                
                if let skView = view{
                    skView.showsFPS = true
                    skView.showsNodeCount = true
                    
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView.ignoresSiblingOrder = true
                    
                    /* Set the scale mode to scale to fit the window */
                    scene.scaleMode = .AspectFill
                    
                    skView.presentScene(scene)
                }
                
                
            }
            /*let gameOverScene = GameScene(fileNamed: "GameOverScene")
            
            gameOverScene?.scaleMode = SKSceneScaleMode.AspectFill
            
            self.scene?.view?.presentScene(gameOverScene)
            */
        }else if(gameOverStatus == "Level Won"){
            if let scene = GameWinScene(fileNamed:"GameWinScene") {
                // Configure the view.
                
                if let skView = view{
                    skView.showsFPS = true
                    skView.showsNodeCount = true
                    
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView.ignoresSiblingOrder = true
                    
                    /* Set the scale mode to scale to fit the window */
                    scene.scaleMode = .AspectFill
                    
                    skView.presentScene(scene)
                }
                
                
            }

        }
    }
    func checkJumpContacts() {
        //print("hit the ground")
        if playerStatus == playerStates.jumpWhileRunningRight.rawValue || playerStatus == playerStates.runningRight.rawValue {
            
            move("facingRight")
            playerStatus = playerStates.runningRight.rawValue
            isJumping = false
            
        } else if playerStatus == playerStates.jumpWhileRunningLeft.rawValue || playerStatus == playerStates.runningLeft.rawValue {
            
            move("facingLeft")
            playerStatus = playerStates.runningLeft.rawValue
            isJumping = false
            
        }else if playerStatus == playerStates.jumpWhileStandingAndFacingRight.rawValue || playerStatus == playerStates.standFacingRight.rawValue {
            
            stopMoving("facingRight")
            playerStatus = playerStates.standFacingRight.rawValue
            isJumping = false
            
        }else if playerStatus == playerStates.jumpWhileStandingAndFacingLeft.rawValue || playerStatus == playerStates.standFacingLeft.rawValue {
            
            
            stopMoving("facingLeft")
            playerStatus = playerStates.standFacingLeft.rawValue
            isJumping = false
            
        }
        
    }
    
    func checkObstacleContacts() {
        
        
        if playerStatus == playerStates.jumpWhileRunningRight.rawValue || playerStatus == playerStates.runningRight.rawValue {
            
            stopMoving("facingRight")
            playerStatus = playerStates.standFacingRight.rawValue
            isJumping = false
            
        } else if playerStatus == playerStates.jumpWhileRunningLeft.rawValue || playerStatus == playerStates.runningLeft.rawValue {
            
            stopMoving("facingLeft")
            playerStatus = playerStates.standFacingLeft.rawValue
            isJumping = false
            
        } else if playerStatus == playerStates.jumpWhileStandingAndFacingRight.rawValue || playerStatus == playerStates.standFacingRight.rawValue {
            
            stopMoving("facingRight")
            playerStatus = playerStates.standFacingRight.rawValue
            isJumping = false
            
            
        } else if playerStatus == playerStates.jumpWhileStandingAndFacingLeft.rawValue || playerStatus == playerStates.standFacingLeft.rawValue {
            
            stopMoving("facingLeft")
            playerStatus = playerStates.standFacingLeft.rawValue
            isJumping = false
            
            
        }
        
        
    }

    
    

    
    //if player facing right have him move right and same for left
    func move(direction: String) {
        
        if direction == "facingRight" {
            
            //create array of movements for right
            let rightAnimation = SKAction.animateWithTextures([rightTexture1, rightTexture2, rightTexture3, rightTexture4], timePerFrame: 0.08)
            let rightAnimationContinuous = SKAction.repeatActionForever(rightAnimation)
            //make action forever of running forever
            let moveRight = SKAction.moveByX(10, y: 0, duration: 0.1)
            let moveRightForEver = SKAction.repeatActionForever(moveRight)
            
            player.runAction(SKAction.group([rightAnimationContinuous,moveRightForEver]), withKey: "runningRight")
            
            
        } else if direction == "facingLeft" {
            
            //create array of movements for left
            let leftAnimation = SKAction.animateWithTextures([leftTexture1, leftTexture2, leftTexture3, leftTexture4], timePerFrame: 0.08)
            let leftAnimationContinuous = SKAction.repeatActionForever(leftAnimation)

            //make action forever of running forever
            let moveLeft = SKAction.moveByX(-10, y: 0, duration: 0.1)
            let moveLeftForEver = SKAction.repeatActionForever(moveLeft)
            
            
            player.runAction(SKAction.group([leftAnimationContinuous,moveLeftForEver]), withKey: "runningLeft")
            
            
        }
        
       //play sound here for running
        
    }
    
    //stop running and stop running animation
    func stopMoving(direction: String){
        if direction == "facingRight"{
            player.removeAllActions()
            player.texture = rightTextureStill
            //have him facing right
        }else if direction == "facingLeft"{
            player.removeAllActions()
            player.texture = leftTextureStill
            //have him facing left
        }
        //stap the sound for running if get to work
    }
    
    //have player jump according to action on player
    func startJump(direction: String){
        if !isJumping{
            
            //if player is moving right giv impulse  to move player right and up
            if direction == "movingRight"{
                player.removeAllActions()
                //set image to jumping right
                player.texture = rightTextureJump
                player.physicsBody?.applyImpulse(CGVectorMake(CGFloat(10), CGFloat(75)))
            }
                //if player is moving left giv impulse to move player right and up
            else if direction == "movingLeft"{
                player.removeAllActions()
                //set image to jumping left
                player.texture = leftTextureStill
                player.physicsBody?.applyImpulse(CGVectorMake(CGFloat(-10), CGFloat(75)))
            }
                //if player is standing still giv impulse to move player up
            else if direction == "standingRight"{
                //print("wtf")
                player.removeAllActions()
                //set image to jumping right
                player.texture = rightTextureJump
                self.player.physicsBody?.applyImpulse(CGVectorMake(0, 75))
            }
                //if player is standing still giv impulse to move player up
            else if direction == "standingLeft"{
                player.removeAllActions()
                //set image to jumping left
                player.texture = leftTextureJump
                player.physicsBody?.applyImpulse(CGVectorMake(CGFloat(0), CGFloat(75)))
            }
            //set jumping to true
            isJumping = true
        }
        
    }
    //stap the player jumping
    func stopJumping(direction: String){
        if direction == "jumpedRight"{
            player.removeAllActions()
            player.texture = rightTextureStill
            //set image to facing rihgt
        }
        else if direction == "jumpedLeft"{
            player.removeAllActions()
            player.texture = leftTextureStill
            //set image to facing left
        }
        isJumping = false
    }
    
    
}

class Enemy: SKSpriteNode {
    
    var right: Bool = true
    
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        let enemyTexture = SKTexture(imageNamed: "enemy")
        super.init(texture: enemyTexture, color: UIColor.whiteColor(), size: enemyTexture.size())
    }
    
    convenience init(initialPosition: CGPoint) {
        self.init()
        self.position = initialPosition
        
        move()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func move() {
        
        if right == true {
            
            self.removeAllActions()
            let moveRight = SKAction.moveByX(4, y: 0, duration: 0.1)
            let moveForEverToRight = SKAction.repeatActionForever(moveRight)
            self.runAction(moveForEverToRight)
            
        } else {
            
            self.removeAllActions()
            let moveLeft = SKAction.moveByX(-4, y: 0, duration: 0.1)
            let moveForEverToLeft = SKAction.repeatActionForever(moveLeft)
            self.runAction(moveForEverToLeft)
            
        }
        
        right = !right
        
    }
    
}
